@extends('layouts.master')

@section('title')
    Detail Cast
@endsection

@section('content')
    <div>
        <h2>Edit Cast {{$cast->nama}}</h2>
        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label for="title">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">

                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="body">Umur</label>
                <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur">
    
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="body">Biografi</label>
                <textarea name="bio" class="form-control" id="bio" cols="30" rows="10"></textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
@endsection