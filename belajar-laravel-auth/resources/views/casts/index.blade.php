@extends('layouts.master')

@section('title')
    Index Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary mb-2">Tambah</a>
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col" style="width : 400px">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>{{$value->umur}}</td>
                <td>{{$value->bio}}</td>
                <td style="display : flex">
                    <a href="/cast/{{$value->id}}" class="btn btn-info mr-2">Show</a>
                    <a href="/cast/{{$value->id}}/edit" class="btn btn-primary mr-2">Edit</a>
                    <form action="/cast/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger mr-2" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>

@endsection